//
//  NotesView.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 3/1/23.
//

import SwiftUI

struct NotesView: View {
    @Environment(\.colorScheme) private var colorScheme
    @Environment(NotesViewModel.self) private var notesVM
    @Namespace private var namespace
    @State private var showSimpleNote: Bool = false
    
    var body: some View {
        NavigationStack {
            ScrollView {
                ForEach(notesVM.notes) { note in
                    NavigationLink(value: note) {
                        NoteRowView(note: note)
                            .matchedTransitionSource(id: note.id, in: namespace) { source in
                                source
                                    .background(colorScheme == .dark ? .black : .white)
                                    .clipShape(RoundedRectangle(cornerRadius: 8))
                                    .shadow(radius: 4)
                            }
                            .swipeActions(allowsFullSwipe: false) {
                                Button(role: .destructive) {
                                    notesVM.removeNote(withId: note.id)
                                    notesVM.fetchNotes()
                                } label: {
                                    Label("Delete", systemImage: "trash")
                                }
                            }
                    }
                }
                .padding()
                .navigationDestination(for: Note.self) { note in
                    SimpleNoteView(note: note, namespace: namespace)
                }
            }
        }
    }
}

//
//  SimpleNoteView.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 7/1/23.
//

import SwiftUI

struct SimpleNoteView: View {
    let note: Note
    let namespace: Namespace.ID
    
    var body: some View {
        VStack {
            TrainCardView(date: note.date, train: note.train1)
            TrainCardView(date: note.date, train: note.train2)
        }
        .navigationTransition(.zoom(sourceID: note.id, in: namespace))
    }
}

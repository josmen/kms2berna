//
//  TrainCardView.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 20/9/24.
//

import SwiftUI

struct TrainCardView: View {
    @Environment(\.colorScheme) private var colorScheme
    let date: Date
    let train: Train

    var body: some View {
        VStack {
            Text(date, format: .dateTime)
                .font(.headline)
                .padding(.vertical, 30)
            Text(String(describing: train.train))
                .font(.largeTitle)
                .fontWeight(.heavy)
                .padding(.bottom, 30)
            Text("Kilómetros: \(train.kms, format: .number)")
                .font(.title)
                .fontWeight(.semibold)
                .padding(.bottom, 10)
            Text("Horas GE: \(train.hours, format: .number)")
                .font(.title)
                .fontWeight(.semibold)
                .padding(.bottom, 10)
        }
        .padding()
        .background(colorScheme == .dark ? .black : .white)
        .cornerRadius(8)
        .shadow(radius: 4)
        .foregroundColor(Color.accentColor)
        .padding(.top, 32)
    }
}

#Preview {
    let date: Date = .init(timeIntervalSince1970: 1670)
    let train: Train = .init(train: 2501, kms: 12345, hours: 12345)
    TrainCardView(date: date, train: train)
}

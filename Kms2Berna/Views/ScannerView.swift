//
//  ContentView.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 28/12/22.
//

import SwiftUI
import VisionKit

struct ScannerView: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(ViewModel.self) private var vm
    @Environment(NotesViewModel.self) private var notesVM
    @State private var recognizedText: String = ""
    
    var body: some View {
        switch vm.dataScannerAccessStatus {
            case .scannerAvailable:
                mainView
                    .onChange(of: recognizedText) { _, newValue in
                        checkMatch(newValue)
                    }
            case .cameraNotAvailable:
                Text("Tu dispositivo no tiene cámara")
            case .scannerNotAvailable:
                Text("Tu dispositivo no soporta escanear texto con esta app")
            case .cameraAccessNotGranted:
                Text("Por favor, permite el acceso a la cámara para esta app en ajustes")
            case .notDetermined:
                Text("Solicitando el acceso a la cámara")
        }
    }
}

extension ScannerView {
    private func checkMatch(_ text: String) {
        let regex1 = /(UNIDAD)\s*((25)(0[1-9]|1[0-2]))/
        if let match = text.firstMatch(of: regex1) {
            if notesVM.train == "" {
                notesVM.train = String(match.2)
            }
        }
        
        let regex2 = /(RECORRIDOS)\s(\d*)/
        if let match = text.firstMatch(of: regex2) {
            if notesVM.kms == "" {
                notesVM.kms = String(match.2)
            }
        }
        
        let regex3 = /(ELECTROGENO)\s(\d*)/
        let regex4 = /(\d*)\n(\d*)\n(\d*)\n(\d*)\n(\d*)\n(\d*)/
        let regex5 = /(\d*)\n(\d*)\n(\d*)\n(\d*)\n(\d*)/
        if let match = text.firstMatch(of: regex3) {
            if notesVM.hours == "" {
                notesVM.hours = String(match.2)
            }
        } else if let match = text.firstMatch(of: regex4) {
            if notesVM.hours == "" {
                notesVM.hours = String(match.2)
            }
        } else if let match = text.firstMatch(of: regex5) {
            if notesVM.hours == "" {
                notesVM.hours = String(match.1)
            }
        }
    }
}

extension ScannerView {
    private var mainView: some View {
        VStack {
            liveImageFeed
                .background { Color.gray.opacity(0.3) }
                .ignoresSafeArea()
                .id(vm.dataScannerViewId)
            bottomContainerView
                .frame(maxHeight: 200)
        }
    }
    
    @ViewBuilder
    private var liveImageFeed: some View {
        @Bindable var vm = vm
        
        DataScannerView(
            recognizedItems: $vm.recognizedItems,
            recognizedDataType: vm.recognizedDataType,
            recognizesMultipleItems: vm.recognizesMultipleItmes)
    }
    
    private var headerView: some View {
        VStack {
            HStack {
                Text(vm.headerText)
                Spacer()
            }
        }
        .padding(.horizontal)
    }
    
    private var recognizedTextView: some View {
        VStack(alignment: .leading) {
            if notesVM.train != "" {
                Text("Tren: \(notesVM.train)")
            }
            if notesVM.kms != "" {
                Text("Kilómetros: \(notesVM.kms)")
            }
            if notesVM.hours != "" {
                Text("Horas GE: \(notesVM.hours)")
            }
        }
        .font(.title3)
        .padding()
    }
    
    @ViewBuilder
    private var bottomContainerView: some View {
        VStack {
            ScrollView {
                LazyVStack(alignment: .leading, spacing: 16) {
                    ForEach(vm.recognizedItems) { item in
                        switch item {
                            case .text(let text):
                                recognizedTextView
                                    .task {
                                        recognizedText = text.transcript
                                    }
                            default:
                                Text("Unknown")
                        }
                    }
                }
                .padding()
            }
            
            Button {
                recognizedText = ""
                vm.recognizedItems = []
                dismiss()
            } label: {
                Text("Hecho")
                    .font(.headline)
                    .fontWeight(.bold)
            }
            .frame(width: 200, height: 40, alignment: .center)
            .foregroundColor(.blue)
            .background(.white)
            .cornerRadius(10)
            .padding(.bottom, 10)
        }
    }
}

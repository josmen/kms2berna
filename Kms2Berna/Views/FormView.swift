//
//  FormView.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 27/9/23.
//

import SwiftUI

struct FormView: View {
    @Environment(NotesViewModel.self) private var notesVM
    @FocusState private var isFocused: Bool
    @State private var showScanner: Bool = false

    var body: some View {
        @Bindable var notesVM = notesVM

        Form {
            TextField("Tren", text: $notesVM.train)
                .keyboardType(.numberPad)
                .focused($isFocused)
            TextField("Kilómetros", text: $notesVM.kms)
                .keyboardType(.numberPad)
                .focused($isFocused)
            TextField("Horas GE", text: $notesVM.hours)
                .keyboardType(.numberPad)
                .focused($isFocused)
            Button("Guardar") {
                notesVM.saveTrain()
                isFocused = false
            }
            .tint(.blue)
            .disabled(!notesVM.isValidForm())
            Button("Limpiar") {
                notesVM.resetForm()
                isFocused = false
            }
            .tint(.red)
        }
        .font(.title2)
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button {
                    notesVM.resetForm()
                    isFocused = false
                    showScanner = true
                } label: {
                    Label("Escanear", systemImage: "scanner")
                }
            }
        }
        .fullScreenCover(isPresented: $showScanner) {
            ScannerView()
        }
    }
}

#Preview {
    FormView()
}

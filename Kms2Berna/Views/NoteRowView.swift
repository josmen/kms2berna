//
//  NoteRowView.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 5/1/23.
//

import SwiftUI

struct NoteRowView: View {
    
    let note: Note
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(note.date, style: .date)
                .font(.headline)
                .opacity(0.5)
                
            HStack(alignment: .lastTextBaseline) {
                VStack(alignment: .leading) {
                    Text(String(describing: note.train1.train))
                        .font(.largeTitle)
                        .fontWeight(.light)
                }
                Spacer()
                VStack(alignment: .trailing) {
                    Text("Kilómetros: \(note.train1.kms, format: .number)")
                    Text("Horas GE: \(note.train1.hours, format: .number)")
                }
                .font(.callout)
            }
            
            HStack(alignment: .lastTextBaseline) {
                VStack(alignment: .leading) {
                    Text(String(describing: note.train2.train))
                        .font(.largeTitle)
                        .fontWeight(.light)
                }
                Spacer()
                VStack(alignment: .trailing) {
                    Text("Kilómetros: \(note.train2.kms, format: .number)")
                    Text("Horas GE: \(note.train2.hours, format: .number)")
                }
                .font(.callout)
            }
        }
        .padding(.horizontal, 8)
        .padding()
    }
}

struct NoteRowView_Previews: PreviewProvider {
    static var previews: some View {
        let oddTrain = Train(train: 2501, kms: 123456789, hours: 1789)
        let evenTrain = Train(train: 2502, kms: 987654321, hours: 1987)
        let note = Note(train1: oddTrain, train2: evenTrain)
        NoteRowView(note: note)
            .previewLayout(.sizeThatFits)
    }
}

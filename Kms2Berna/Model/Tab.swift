//
//  Tab.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 27/9/23.
//

import Foundation

enum Tab: String, CaseIterable {
    case capture = "photo.stack"
    case notes = "doc.on.doc"
    
    var title: String {
        switch self {
            case .capture:
                return "Captura"
            case .notes:
                return "Últimas lecturas"
        }
    }
}

struct AnimatedTab: Identifiable {
    var id: UUID = .init()
    var tab: Tab
    var isAnimating: Bool?
}

//
//  Note.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 5/1/23.
//

import Foundation

struct Note: Codable, Hashable, Identifiable {
    var id: UUID = .init()
    let date: Date
    let train1: Train
    let train2: Train
    
    init(train1: Train, train2: Train) {
        self.date = .now
        self.train1 = train1
        self.train2 = train2
    }
}

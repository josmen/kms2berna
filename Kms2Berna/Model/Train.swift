//
//  Train.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 19/9/24.
//


import Foundation

struct Train: Codable, Hashable, Identifiable {
    var id: UUID = .init()
    let train: Int
    let kms: Int
    let hours: Int
}

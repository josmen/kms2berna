//
//  ViewModel.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 28/12/22.
//

import AVKit
import VisionKit

enum ScanType: String {
    case text
}

enum DataScannerAccessStatusType {
    case notDetermined
    case cameraAccessNotGranted
    case cameraNotAvailable
    case scannerAvailable
    case scannerNotAvailable
}

@Observable
final class ViewModel {
    
    var dataScannerAccessStatus: DataScannerAccessStatusType = .notDetermined
    var recognizedItems: [RecognizedItem] = []
    var scanType: ScanType = .text
    var textContentType: DataScannerViewController.TextContentType?
    var recognizesMultipleItmes = false
    
    var recognizedDataType: DataScannerViewController.RecognizedDataType {
        .text(textContentType: textContentType)
    }
    
    var headerText: String {
        if recognizedItems.isEmpty {
            return "Escaneando \(scanType.rawValue)"
        } else {
            return "Reconocidos \(recognizedItems.count) item(s)"
        }
    }
    
    var dataScannerViewId: Int {
        var hasher = Hasher()
        hasher.combine(scanType)
        hasher.combine(recognizesMultipleItmes)
        if let textContentType {
            hasher.combine(textContentType)
        }
        return hasher.finalize()
    }
    
    @MainActor
    private var isScannerAvailable: Bool {
        DataScannerViewController.isAvailable && DataScannerViewController.isSupported
    }
    
    func requestDataScannerAccessStatus() async {
        guard await UIImagePickerController.isSourceTypeAvailable(.camera) else {
            dataScannerAccessStatus = .cameraNotAvailable
            return
        }
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            dataScannerAccessStatus = await isScannerAvailable ? .scannerAvailable : .scannerNotAvailable
        case .restricted, .denied:
            dataScannerAccessStatus = .cameraAccessNotGranted
        case .notDetermined:
            let granted = await AVCaptureDevice.requestAccess(for: .video)
            if granted {
                dataScannerAccessStatus = await isScannerAvailable
                    ? .scannerAvailable
                    : .scannerNotAvailable
            } else {
                dataScannerAccessStatus = .cameraAccessNotGranted
            }
        default: break
        }
    }
}

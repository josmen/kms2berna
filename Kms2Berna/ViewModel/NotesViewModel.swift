//
//  NotesViewModel.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 5/1/23.
//

import Foundation

@Observable
final class NotesViewModel {
    var notes: [Note] = []
    
    var evenTrain: Train?
    var oddTrain: Train?
    
    var train: String = ""
    var kms: String = ""
    var hours: String = ""
    
    private let userDefaults: UserDefaults = .standard
    
    init() {
        fetchNotes()
    }
    
    func isValidForm() -> Bool {
        !self.train.isEmpty &&
        !self.kms.isEmpty &&
        !self.hours.isEmpty &&
        self.train.isNumeric &&
        self.train.isValidTrainNumber &&
        self.kms.isNumeric &&
        self.hours.isNumeric
    }
    
    func resetForm() {
        self.train = ""
        self.kms = ""
        self.hours = ""
    }
    
    private func getAllNotes() -> [Note] {
        if let notesData = userDefaults.object(forKey: "notes") as? Data {
            if let notes = try? JSONDecoder().decode([Note].self, from: notesData) {
                return notes
            }
        }
        return []
    }
    
    func fetchNotes() {
        notes = getAllNotes()
    }
    
    func saveTrain() {
        guard isValidForm() else { return }
        
        if self.train.isEven {
            evenTrain = Train(train: Int(self.train)!, kms: Int(self.kms)!, hours: Int(self.hours)!)
        } else {
            oddTrain = Train(train: Int(self.train)!, kms: Int(self.kms)!, hours: Int(self.hours)!)
        }
        
        resetForm()
        
        if evenTrain != nil, oddTrain != nil {
            saveNote()
        }
    }
    
    private func saveNote() {
        guard let evenTrain, let oddTrain else { return }
        let newNote = Note(train1: oddTrain, train2: evenTrain)
        notes.insert(newNote, at: 0)
        encodeAndSaveAllNotes()
    }
    
    func removeNote(withId id: UUID) {
        notes.removeAll(where: { $0.id == id })
        encodeAndSaveAllNotes()
    }
    
    private func encodeAndSaveAllNotes() {
        if let encoded = try? JSONEncoder().encode(notes) {
            userDefaults.set(encoded, forKey: "notes")
        }
    }
}

//
//  Kms2BernaApp.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 28/12/22.
//

import SwiftUI

@main
struct Kms2BernaApp: App {
    @State private var vm = ViewModel()
    @State private var notesVM = NotesViewModel()
    
    var body: some Scene {
        WindowGroup {
            RootView()
                .environment(vm)
                .environment(notesVM)
                .task {
                    await vm.requestDataScannerAccessStatus()
                }
//                .preferredColorScheme(.dark)
        }
    }
}

//
//  String+Extensions.swift
//  Kms2Berna
//
//  Created by Jose Antonio Mendoza on 5/1/23.
//

import Foundation

extension String {
    
    var isNumeric: Bool {
        Double(self) != nil
    }
    
    var isEven: Bool {
        if let number = Double(self) {
            return Int(number) % 2 == 0
        }
        return false
    }
    
    var isValidTrainNumber: Bool {
        if let number = Double(self) {
            return Int(number) >= 2501 && Int(number) <= 2512
        }
        return false
    }
}
